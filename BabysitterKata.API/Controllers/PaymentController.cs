﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace BabysitterKata.API.Controllers
{
    [Route("api/[controller]")]
    public class PaymentController : Controller
    {
        private readonly IBabysitterPayService _payService;

        public PaymentController()
        {
            _payService = new BabysitterPayService();
        }

        // Get api/payment
        [HttpGet]
        public int GetPayment(int payBeforeBedtime, int payAfterBedtime, int payAfterMidnight, string bedtime, string startTime, string endTime)
        {
            int result = 0;

            if (payBeforeBedtime >= 0 && payAfterBedtime >= 0 && payAfterMidnight >= 0)
            {
                _payService.SetPayRate(payBeforeBedtime, payAfterBedtime, payAfterMidnight);

                DateTime.TryParse(bedtime,   out DateTime parsedBedtime);
                DateTime.TryParse(startTime, out DateTime parsedStartTime);
                DateTime.TryParse(endTime,   out DateTime parsedEndTime);

                if (   parsedBedtime   > DateTime.MinValue 
                    && parsedStartTime > DateTime.MinValue 
                    && parsedEndTime   > DateTime.MinValue)
                {
                    _payService.SetBedtime(parsedBedtime);
                    _payService.SetHoursWorked(parsedStartTime, parsedEndTime);

                    return _payService.GetPayment();
                }
            }

            return result;
        }
    }
}
