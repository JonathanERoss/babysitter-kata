﻿namespace BabysitterKata
{
    public class Person
    {
        private HoursWorked TheHoursWorked { get; }

        private PayRate RateOfPay { get; }

        public Person(HoursWorked hours, PayRate rate)
        {
            TheHoursWorked = hours;
            RateOfPay = rate;
        }

        public int CalculatePaymentDue()
        {
            int result = 0;

            result = TheHoursWorked.StartToBedtime * RateOfPay.StartToBedtimeRate;
            result += TheHoursWorked.BedtimeToMidnight * RateOfPay.BedtimeToMidnightRate;
            result += TheHoursWorked.MidnightToEnd * RateOfPay.MidnightToEndRate;

            return result;
        }
    }
}
