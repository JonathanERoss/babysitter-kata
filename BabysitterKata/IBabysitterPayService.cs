﻿using System;

namespace BabysitterKata
{
    public interface IBabysitterPayService
    {
        void SetPayRate(int payBeforeBedtime, int payAfterBedtime, int payAfterMidnight);
        void SetBedtime(DateTime bedTime);
        void SetHoursWorked(DateTime startTime, DateTime endTime);
        int GetPayment();
    }
}