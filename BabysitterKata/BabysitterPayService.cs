﻿using System;

namespace BabysitterKata
{
    public class BabysitterPayService : IBabysitterPayService
    {
        private HoursWorked _HoursWorked;

        private PayRate _PayRate;

        private WorkSchedule _WorkSchedule;
        
        public void SetPayRate(int payBeforeBedtime, int payAfterBedtime, int payAfterMidnight)
        {
            _PayRate = new PayRate(payBeforeBedtime, payAfterBedtime, payAfterMidnight);
        }
        
        public void SetBedtime(DateTime bedTime)
        {
            _WorkSchedule = new WorkSchedule(bedTime);
        }
        
        public void SetHoursWorked(DateTime startTime, DateTime endTime)
        {
            _HoursWorked = _WorkSchedule.CalculateHoursInEachPaySegment(startTime, endTime);
        }
        
        public int GetPayment()
        {
            Person person = new Person(_HoursWorked, _PayRate);
            return person.CalculatePaymentDue();
        }
        
    }
}
