﻿namespace BabysitterKata
{
    public class HoursWorked
    {
        public int StartToBedtime { get; }

        public int BedtimeToMidnight { get; }

        public int MidnightToEnd { get; }

        public HoursWorked(int startToBedtime, int bedtimeToMidnight, int midnightToEnd)
        {
            StartToBedtime = startToBedtime;
            BedtimeToMidnight = bedtimeToMidnight;
            MidnightToEnd = midnightToEnd;
        }

    }
}
