﻿using System;

namespace BabysitterKata
{
    public class PayRate
    {
        public int StartToBedtimeRate { get; }

        public int BedtimeToMidnightRate { get; }

        public int MidnightToEndRate { get; }


        public PayRate(int startToBedtimeRate, int bedtimeToMidnightRate, int midnightToEndRate)
        {
            StartToBedtimeRate = startToBedtimeRate;
            BedtimeToMidnightRate = bedtimeToMidnightRate;
            MidnightToEndRate = midnightToEndRate;
        }

    }
}
