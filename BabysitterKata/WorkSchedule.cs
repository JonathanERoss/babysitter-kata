﻿using System;

namespace BabysitterKata
{
    public class WorkSchedule
    {
        private DateTime Bedtime { get; }

        public WorkSchedule(DateTime bedtime)
        {
            Bedtime = bedtime;
        }

        public HoursWorked CalculateHoursInEachPaySegment(DateTime startWork, DateTime endWork)
        {              
            int hoursBeforeBedtime = Bedtime.Hour - startWork.Hour;
            
            int hoursAfterMidnight = endWork.Date > startWork.Date 
                ? endWork.Hour 
                : 0;
            
            int hoursBedtimeToMidnight = endWork.Date > startWork.Date
                ? 24 - Bedtime.Hour
                : endWork.Hour - Bedtime.Hour;
            
            return new HoursWorked(hoursBeforeBedtime, hoursBedtimeToMidnight, hoursAfterMidnight);
        }

    }
}
