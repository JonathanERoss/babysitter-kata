using System.Diagnostics.CodeAnalysis;
using BabysitterKata.API.Controllers;
using NUnit.Framework;

namespace BabysitterKata.API.Tests
{
    [ExcludeFromCodeCoverage]
    public class PaymentControllerTests
    {
        private PaymentController _testController;

        [SetUp]
        public void Setup()
        {
            _testController = new PaymentController();
        }
        
        [TestCase(12, 8, 16, "invalid", "date", "test", 0)]
        [TestCase(12, 8, 16, "05-25-2018 19:00:00", "05-25-2018 17:00:00", "05-25-2018 22:00:00", 48)]
        [TestCase(12, 8, 16, "05-25-2018 19:00:00", "05-25-2018 17:00:00", "05-26-2018 03:00:00", 112)]
        public void CalculatePayment(int payBeforeBedtime, int payAfterBedtime, int payAfterMidnight, string bedtime, string startTime, string endTime, int expectedResult)
        {
            int actualResult = _testController.GetPayment(payBeforeBedtime, payAfterBedtime, payAfterMidnight, bedtime, startTime, endTime);
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}