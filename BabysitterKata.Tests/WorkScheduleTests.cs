using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BabysitterKata.Tests
{
    [ExcludeFromCodeCoverage]
    public class WorkScheduleTests
    {

        private WorkSchedule _testSchedule;

        [SetUp]
        public void Setup()
        {
            _testSchedule = new WorkSchedule(DateTime.Parse("05-25-2018 19:00:00"));
        }
        
        [Test]
        public void CalculateHoursWorkedWhenEndIsBeforeMidnight()
        {
            HoursWorked actualHoursWorked = 
                _testSchedule.CalculateHoursInEachPaySegment(
                    DateTime.Parse("05-25-2018 17:00:00"),
                    DateTime.Parse("05-25-2018 22:00:00")
                );

            HoursWorked expectedHoursWorked = new HoursWorked(2, 3, 0);

            Assert.IsTrue(AreHoursWorkedEqual(expectedHoursWorked, actualHoursWorked));
        }

        [Test]
        public void CalculateHoursWorkedWhenEndIsAfterMidnight()
        {
            HoursWorked actualHoursWorked =
                _testSchedule.CalculateHoursInEachPaySegment(
                    DateTime.Parse("05-25-2018 17:00:00"),
                    DateTime.Parse("05-26-2018 03:00:00")
                );

            HoursWorked expectedHoursWorked = new HoursWorked(2, 5, 3);
            
            Assert.IsTrue(AreHoursWorkedEqual(expectedHoursWorked, actualHoursWorked));
        }

        private bool AreHoursWorkedEqual(HoursWorked a, HoursWorked b)
        {
            return Equals(a.StartToBedtime, b.StartToBedtime)
                   && Equals(a.BedtimeToMidnight, b.BedtimeToMidnight)
                   && Equals(a.MidnightToEnd, b.MidnightToEnd);
        }
    }
}