using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BabysitterKata.Tests
{
    [ExcludeFromCodeCoverage]
    public class BabysitterPayServiceTests
    {

        private IBabysitterPayService _service;

        [SetUp]
        public void Setup()
        {
            _service = new BabysitterPayService();
            _service.SetPayRate(12, 8, 16);
            _service.SetBedtime(DateTime.Parse("05-25-2018 19:00:00"));
        }
        
        [Test]
        public void CalculatePaymentWhenEndIsBeforeMidnight()
        {
            _service.SetHoursWorked(DateTime.Parse("05-25-2018 17:00:00"), DateTime.Parse("05-25-2018 22:00:00"));

            int actualPay = _service.GetPayment();
            int expectedPay = 48;

            Assert.AreEqual(expectedPay, actualPay);
        }
        
        [Test]
        public void CalculatePaymentWhenEndIsAfterMidnight()
        {
            _service.SetHoursWorked(DateTime.Parse("05-25-2018 17:00:00"), DateTime.Parse("05-26-2018 03:00:00"));

            int actualPay = _service.GetPayment();
            int expectedPay = 112;

            Assert.AreEqual(expectedPay, actualPay);
        }
    }
}