using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BabysitterKata.Tests
{
    [ExcludeFromCodeCoverage]
    public class PersonTests
    {
        private Person _testPerson;
        private PayRate _testPayRate;

        [SetUp]
        public void Setup()
        {
            _testPayRate = new PayRate(12, 8, 16);       
        }

        [Test]
        public void PersonIsPaidFollowingRatePerHourBeforeBedtime()
        {
            // paid for 2 hours
            HoursWorked testHours = new HoursWorked(2, 0, 0);
            _testPerson = new Person(testHours, _testPayRate);

            int actualPay = _testPerson.CalculatePaymentDue();

            int expectedPay = 24;
            Assert.AreEqual(expectedPay, actualPay, "Paid for 2 hours");

            // paid for 3 hours
            testHours = new HoursWorked(3, 0, 0);
            _testPerson = new Person(testHours, _testPayRate);

            actualPay = _testPerson.CalculatePaymentDue();
            expectedPay = 36;
            Assert.AreEqual(expectedPay, actualPay, "Paid for 3 hours");
        }

        [Test]
        public void PersonIsPaidFollowingRatePerHourBeforeBedtimeAndAfterBedtime()
        {
            // paid for 1 hours before, 1 hour after bedtime
            HoursWorked testHours = new HoursWorked(1, 1, 0);
            _testPerson = new Person(testHours, _testPayRate);

            int actualPay = _testPerson.CalculatePaymentDue();

            int expectedPay = 20;
            Assert.AreEqual(expectedPay, actualPay, "Paid for 1 before and 1 after bedtime");

            // paid for 1 hours before, 2 hour after bedtime
            testHours = new HoursWorked(1, 2, 0);
            _testPerson = new Person(testHours, _testPayRate);

            actualPay = _testPerson.CalculatePaymentDue();

            expectedPay = 28;
            Assert.AreEqual(expectedPay, actualPay, "Paid for 1 before and 2 after bedtime");
        }

        [Test]
        public void PersonIsPaidFollowingRatePerHourBeforeMidnightAndAfterMidnight()
        {
            // paid for 1 hours before bed, 3 hour after bedtime, and 1 hour after midnight
            HoursWorked testHours = new HoursWorked(1, 3, 1);
            _testPerson = new Person(testHours, _testPayRate);

            int actualPay = _testPerson.CalculatePaymentDue();

            int expectedPay = 52;
            Assert.AreEqual(expectedPay, actualPay, "Paid for 1 hours before bed, 3 hour after bedtime, and 1 hour after midnight");
        }
    }
}