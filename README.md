# Babysitter Kata

This repository contains my solution to the Code Kata programming exercise described [HERE](docs/KataSource.md)

## Overview
The solution was written as a .NET Core 2.0 class library using C#.  There are two projects. One for the class library and the other for unit tests.

- BabysitterKata
- BabysitterKata.Tests
- BabysitterKata.API
- BabysitterKata.API.Tests

The implementation contains a service called **BabysitterPayService** which provides an interface **IBabysitterPayService**.  Using this service, we can perform functions such as setting the pay rate, hours, and the bedtime.  Once these are set we can calculate how much the babysitter should be paid.  

I have also provided a basic WebAPI project, **BabysitterKata.API**, with tests that provides an example for consuming the service and providing calculated payment due.

To use the service, we first set the pay rate using the **SetPayRate** method.  Once this is set, we can set the bedtime uaing the **SetBedtime** method.  Final, we set the hours worked using the **SetHoursWorked** method.  After everything is set, calling the **GetPayment** method will return the calculated amount that the babysitter should be paid.  The tests in the **BabysitterPayServiceTests** class provide examples of this.

## Running The Tests

From the command line, run the following in the root of the repository:

    dotnet test .\BabysitterKata.Tests\BabysitterKata.Tests.csproj --verbosity normal
    dotnet test .\BabysitterKata.API.Tests\BabysitterKata.API.Tests.csproj --verbosity normal

This will build the solution, then execute the tests.  You may also build using the following commands.

	dotnet clean
	dotnet build

The build step will fetch all nuget packages required of the projects.  It is not necessary, but to manually restore the packages simply execute the following command:

    dotnet restore

You may also open the solution file (Babysitter-Kata.sln) with Visual Studio and run tests.

## Test Results and Coverage

![Test Results](docs/TestResults.png)


## Author
- Jonathan E. Ross
